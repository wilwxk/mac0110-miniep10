function checa_soma(m, li, lj, ri, rj)
    s = 0
    for i = li:ri
        for j = lj:rj
            s += m[i, j]
        end
    end
    return s
end

function max_sum_submatrix(m)
    best = m[1, 1]
    n = size(m)[1]
    ans = Vector{Array{Int64,2}}()
    for k = 1:n
        for i = 1:n-k+1
            for j = 1:n-k+1
                val = checa_soma(m, i, j, i+k-1, j+k-1)
                if val > best
                    best = val
                    ans = Vector{Array{Int64,2}}()
                end
                if val == best
                    push!(ans, m[i:i+k-1, j:j+k-1])
                end
            end
        end
    end
    return ans
end
